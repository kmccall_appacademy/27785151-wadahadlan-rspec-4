class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def zeros(num)
    num > 10 ? num.to_s : "0#{num}"
  end

  def seconds_sq
    seconds % 60
  end

  def minutes
    (seconds % 3600) / 60
  end

  def hours
    seconds / 3600
  end

  def time_string
    "#{zeros(hours)}:#{zeros(minutes)}:#{zeros(seconds_sq)}"
  end
end
