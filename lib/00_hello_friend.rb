class Friend
  attr_reader :name
  # TODO: your code goes here!
  def initialize(name = "")
    @name = name
  end

  def greeting(name = nil)
    if name
      "Hello, #{name}!"
    else
      "Hello!"
    end
  end
end
