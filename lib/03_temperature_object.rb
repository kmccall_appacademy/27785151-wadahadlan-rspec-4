class Temperature
  def initialize(opt = {})
    @temperature = opt
  end

  def in_celsius
    if @temperature.keys[0] == :c
      @temperature.values[0]
    else
      ((@temperature.values[0] - 32) / 1.8).round(1)
    end
  end

  def in_fahrenheit
    if @temperature.keys[0] == :f
      @temperature.values[0]
    else
      (@temperature.values[0] * 1.8 + 32).round(1)
    end
  end

  def self.from_celsius(temp)
    new(c: temp)
  end

  def self.from_fahrenheit(temp)
    new(f: temp)
  end
end

class Celsius < Temperature
  def initialize(temp)
    @temperature = { c: temp }
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @temperature = { f: temp }
  end
end
