class Book
  # TODO: your code goes here!
  SMALL_WORDS = [
    "the",
    "a",
    "and",
    "an",
    "of",
    "in"
  ]

  attr_reader :title
  def title=(title)
    title_words = title.downcase.split
    new_title = title_words.map.with_index do |word, idx|
      if SMALL_WORDS.include?(word) && idx != 0
        word
      else
        word[0].upcase + word[1..-1]
      end
    end
    @title = new_title.join(' ')
  end
end
